package com.example.kotlin_learning.category.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.example.kotlin_learning.R
import com.example.kotlin_learning.category.data.Category
import com.example.kotlin_learning.utils.Define
import kotlinx.android.synthetic.main.item_category.view.*
import kotlinx.android.synthetic.main.item_image_recyclervew.view.*

class CategoryViewPagerAdapter(val context: Context): RecyclerView.Adapter<CategoryViewPagerAdapter.PagerViewHolder>() {

    val categories = Define.categories


    class PagerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PagerViewHolder =
        PagerViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_category, parent, false))

    override fun onBindViewHolder(holder: PagerViewHolder, position: Int) {

        Glide.with(context)
            .load(categories[position].imageUrl)
            .centerCrop()
            .transition(DrawableTransitionOptions.withCrossFade())
            .error(R.drawable.ic_error)
            .into(holder.itemView.itemImgCategory)

        holder.itemView.itemTxtCategoryName.text = categories[position].name
    }

    override fun getItemCount(): Int {
        return categories.count()
    }
}
