package com.example.kotlin_learning.activities

import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.kotlin_learning.R
import com.example.kotlin_learning.account.ui.AccountFragment
import com.example.kotlin_learning.category.ui.CategoryFragment
import com.example.kotlin_learning.home.ui.HomeFragment
import com.example.kotlin_learning.noti.data.AppDatabase
import com.example.kotlin_learning.noti.data.Notification
import com.example.kotlin_learning.noti.data.NotificationDao
import com.example.kotlin_learning.noti.data.NotificationReceiver
import com.example.kotlin_learning.noti.interfaces.OnNotificationClick
import com.example.kotlin_learning.noti.ui.NotificationFragment
import com.example.kotlin_learning.utils.Define
import com.google.android.material.badge.BadgeDrawable
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var broadcastReceiver: NotificationReceiver
    private lateinit var notificationFragment: NotificationFragment
    private lateinit var db: AppDatabase
    private lateinit var dao: NotificationDao
    private lateinit var naviBottom: BottomNavigationView
    private lateinit var badgeNotification: BadgeDrawable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        db = AppDatabase.getInMemoryDatabase(applicationContext)!!
        dao = db.notiDao()
        naviBottom = navBottom
        badgeNotification = naviBottom.getOrCreateBadge(naviBottom.menu.getItem(2).itemId)

        notificationFragment = NotificationFragment(object : OnNotificationClick {
            override fun onNotificationClicked(notification: Notification?) {
                updateBageNumber()
                Log.wtf("Update Badge","${badgeNotification.number}")
            }

        })

        broadcastReceiver = NotificationReceiver()
        broadcastReceiver.setOnNotificationReceive(notificationFragment.getOnNotificationReceive())
        val intentFilter = IntentFilter()
        intentFilter.addAction(Define.ACTION_BROADCAST_NOTIFICATION)
        registerReceiver(broadcastReceiver, intentFilter)
        Log.wtf("Notification", "Created")

        badgeNotification.isVisible = true
        when (dao.loadNumberOfUnread(false)) {
            0 -> badgeNotification.isVisible = false
            else -> badgeNotification.number = dao.loadNumberOfUnread(false)
        }

        navBottom.setOnNavigationItemSelectedListener(
            BottomNavigationView.OnNavigationItemSelectedListener { item ->
                when (item.itemId) {
                    R.id.navigation_home -> {
                        supportActionBar?.title = "Home"
                        supportFragmentManager.beginTransaction()
                            .replace(R.id.mainFragment, HomeFragment())
                            .commit()
                        return@OnNavigationItemSelectedListener true
                    }

                    R.id.navigation_category -> {
                        supportActionBar?.title = "Categories"
                        supportFragmentManager.beginTransaction()
                            .replace(R.id.mainFragment, CategoryFragment())
                            .commit()
                        return@OnNavigationItemSelectedListener true
                    }

                    R.id.navigation_notifications -> {
                        supportActionBar?.title = "Notification"
                        badgeNotification.number = 0
                        badgeNotification.isVisible = false
                        supportFragmentManager.beginTransaction()
                            .replace(R.id.mainFragment, notificationFragment)
                            .commit()
                        return@OnNavigationItemSelectedListener true
                    }

                    R.id.navigation_account -> {
                        supportActionBar?.title = "Account"
                        supportFragmentManager.beginTransaction()
                            .replace(R.id.mainFragment, AccountFragment())
                            .commit()
                        return@OnNavigationItemSelectedListener true
                    }

                }
                return@OnNavigationItemSelectedListener false
            })

        supportFragmentManager.beginTransaction()
            .replace(R.id.mainFragment, HomeFragment())
            .commit()
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(broadcastReceiver)
    }

    fun updateBageNumber() {
        when (dao.loadNumberOfUnread(false)) {
            0 -> badgeNotification.isVisible = false
            else -> {
                badgeNotification.number = dao.loadNumberOfUnread(false)
                badgeNotification.isVisible = true
            }
        }
    }

}



