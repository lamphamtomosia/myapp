package com.example.kotlin_learning.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.example.kotlin_learning.R

class GreetingActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_greeting)
        Handler(Looper.getMainLooper()).postDelayed({
            val authorization = getSharedPreferences(resources.getString(R.string.authorization), MODE_PRIVATE)

            val authority = authorization.getString(resources.getString(R.string.author), null)

            if (authority == null || authority.toInt() == 0) {
                startActivity(Intent(this, LoginActivity::class.java))
            }else if (authority.toInt() == 1)
                startActivity(Intent(this, MainActivity::class.java))

            finish()
        }, 2000)

    }
}