package com.example.kotlin_learning.account.data

import androidx.room.Dao
import androidx.room.Query

@Dao
interface AccountDao {
    @Query("SELECT * FROM  Account")
    fun getAll(): List<Account>
    @Query("SELECT * FROM Account WHERE username")
    fun getUsername(): Account
}