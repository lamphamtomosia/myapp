package com.example.kotlin_learning.service.firebase

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.example.kotlin_learning.activities.MainActivity
import com.example.kotlin_learning.R
import com.example.kotlin_learning.utils.Define
import com.google.firebase.FirebaseApp
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import java.text.SimpleDateFormat
import java.util.*


class MyFireBaseService : FirebaseMessagingService() {

    val TAG = "FirebaseService "

    override fun onMessageReceived(p0: RemoteMessage) {
        super.onMessageReceived(p0)

        if (p0.data != null && p0.notification != null ) {

            if(p0.data.toList().isNotEmpty()){
                sendNotification(p0.data.values.toList()[0])
                p0.notification!!.title?.let { p0.notification!!.body?.let { it1 ->
                    sendBroadcastNotification(it,
                        it1
                    )
                } }
            }
        }

    }

    override fun onNewToken(p0: String) {
        super.onNewToken(p0)
        sendRegistrationToServer(p0)
    }

    fun sendRegistrationToServer(token: String){

    }


    private fun sendNotification(messageBody: String) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
        val channelId = FirebaseApp.getInstance().options.projectId
        val defaultSoundUri: Uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = channelId?.let {
            NotificationCompat.Builder(this, it)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setLargeIcon(
                    BitmapFactory.decodeResource(
                        resources,
                        R.drawable.ic_launcher_background
                    )
                )
                .setContentTitle(getString(R.string.app_name))
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .setPriority(NotificationManager.IMPORTANCE_HIGH)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .addAction(
                    NotificationCompat.Action(
                        R.drawable.ic_cancel,
                        "Cancel",
                        PendingIntent.getActivity(
                            this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT
                        )
                    )
                )
                .addAction(
                    NotificationCompat.Action(
                        R.drawable.ic_ok,
                        "OK",
                        PendingIntent.getActivity(
                            this,
                            0,
                            intent,
                            PendingIntent.FLAG_CANCEL_CURRENT
                        )
                    )
                )
        }

        val notificationManager: NotificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager


        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                channelId,
                "Channel human readable title",
                NotificationManager.IMPORTANCE_HIGH
            )
            notificationManager.createNotificationChannel(channel)
        }
        notificationManager.notify(0, notificationBuilder?.build())
    }

    fun sendBroadcastNotification(title: String, message: String){
        val actionName = Define.ACTION_BROADCAST_NOTIFICATION
        val intent = Intent(actionName).setAction(actionName)
        val time = SimpleDateFormat("dd/M/yyyy hh:mm:ss").format(Date())

        intent.putExtra("notification", com.example.kotlin_learning.noti.data.Notification(0, title, message, false, time))
        sendBroadcast(intent)
        Log.wtf("Notification ", "Sent")
    }
}