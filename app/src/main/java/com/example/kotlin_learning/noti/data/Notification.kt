package com.example.kotlin_learning.noti.data

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity
@Parcelize
data class Notification(
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name = "title") val title: String?,
    @ColumnInfo(name = "message") val message: String?,
    @ColumnInfo(name = "isOpened") var status: Boolean?,
    @ColumnInfo(name = "time") val time: String?
): Parcelable{
}