package com.example.kotlin_learning.noti.interfaces

interface OnNotiReceive {
    fun sendNotification()
}