package com.example.kotlin_learning.noti.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.kotlin_learning.R
import com.example.kotlin_learning.noti.interfaces.OnDeleteNotification
import com.example.kotlin_learning.noti.interfaces.OnNotiReceive
import com.example.kotlin_learning.noti.interfaces.OnNotificationClick
import com.example.kotlin_learning.noti.interfaces.OnNotificationOptionClick
import com.example.kotlin_learning.noti.adapter.NotificationRecyclerViewAdapter
import com.example.kotlin_learning.noti.data.*
import kotlinx.android.synthetic.main.fragment_notification.*


class NotificationFragment(val onUpdateBadgeClick: OnNotificationClick) : Fragment(), OnNotiReceive, OnDeleteNotification {
    lateinit var db: AppDatabase
    lateinit var dao: NotificationDao
    lateinit var listNotifications: ArrayList<Notification>
    lateinit var onNotificationClick: OnNotificationClick
    lateinit var onNotificationOptionClick: OnNotificationOptionClick
    lateinit var bottomSheetFragment: BottomSheetFragment
    lateinit var notificationRecyclerViewAdapter: NotificationRecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_notification, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        db = context?.let { AppDatabase.getInMemoryDatabase(it) }!!
        dao = db.notiDao()

        listNotifications = dao?.getAll() as ArrayList<Notification>


        onNotificationClick = object : OnNotificationClick {
            override fun onNotificationClicked(notification: Notification?) {
                notification?.status?.let {
                    dao.update(it, notification.id)
                    Log.wtf("Item Clicked", "${notification.id} + ${notification.status}")
                    onUpdateBadgeClick.onNotificationClicked(notification)
                    updateView()
                }
            }
        }


        onNotificationOptionClick = object : OnNotificationOptionClick {
            override fun onOptionClick(notification: Notification) {
                bottomSheetFragment = BottomSheetFragment(notification)
                bottomSheetFragment.setOnDeletenotification(getOnDeleteNotification())
                bottomSheetFragment.show(parentFragmentManager, null)
            }
        }

        notificationRecyclerViewAdapter = NotificationRecyclerViewAdapter(
            requireContext(),
            listNotifications, onNotificationClick,
            onNotificationOptionClick
        )

        recyclerViewNotification.apply {

            adapter = notificationRecyclerViewAdapter

            layoutManager = LinearLayoutManager(
                requireContext(),
                LinearLayoutManager.VERTICAL,
                false
            )

        }

    }

    fun updateView() {
        Log.wtf("DataLoaded", "${listNotifications.size}")
        listNotifications = dao.getAll() as ArrayList<Notification>

        recyclerViewNotification.adapter!!.notifyDataSetChanged()

        Log.wtf("DataLoaded", "${listNotifications.size}")
    }

    fun addNotification(notification: Notification) {
        Log.wtf("DataAdded", "${listNotifications.size}")
        listNotifications.add(0, notification)

        notificationRecyclerViewAdapter.notify(listNotifications)

        Log.wtf("DataAdded", "${listNotifications.size}")
    }

    fun deleteNotification(notification: Notification) {
        Log.wtf("DataDeleted", "${listNotifications.size}")
        listNotifications.remove(notification)

        notificationRecyclerViewAdapter.notify(listNotifications)

        Log.wtf("DataDeleted", "${listNotifications.size}")
    }

    override fun sendNotification() {
        addNotification(dao.getLastNotification())
        onUpdateBadgeClick.onNotificationClicked(null)
    }

    fun getOnNotificationReceive(): OnNotiReceive {
        return this
    }
    fun getOnDeleteNotification(): OnDeleteNotification {
        return this
    }


    override fun onDeleteNotification(notification: Notification) {
        dao.delete(notification)
        bottomSheetFragment.dismiss()
        deleteNotification(notification)
    }


    override fun onStop() {
        super.onStop()
        dao.updateStatus(true,false)
    }

}

