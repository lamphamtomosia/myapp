package com.example.kotlin_learning.noti.data

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface NotificationDao {
    @Query("SELECT * FROM Notification ORDER BY id DESC")
    fun getAll(): List<Notification>

    @Query("SELECT * FROM Notification WHERE id IN (:notiId)")
    fun loadAllByIds(notiId: IntArray): List<Notification>

    @Query("SELECT * FROM Notification ORDER BY id DESC LIMIT 1")
    fun getLastNotification(): Notification

    @Query("SELECT COUNT(id) FROM Notification WHERE isOpened =:isOpened")
    fun loadNumberOfUnread(isOpened: Boolean): Int

    @Query("UPDATE Notification SET isOpened = :isOpened WHERE id = :id")
    fun update(isOpened: Boolean, id: Int)

    @Query("UPDATE Notification SET isOpened = :newStatus WHERE isOpened = :oldStatus")
    fun updateStatus(newStatus: Boolean, oldStatus: Boolean)

    @Insert
    fun insertAll(vararg notifications: Notification)

    @Delete
    fun delete(notification: Notification)

}