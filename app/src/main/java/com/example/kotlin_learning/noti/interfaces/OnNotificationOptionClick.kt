package com.example.kotlin_learning.noti.interfaces

import com.example.kotlin_learning.noti.data.Notification

interface OnNotificationOptionClick {
    fun onOptionClick(notification: Notification)
}