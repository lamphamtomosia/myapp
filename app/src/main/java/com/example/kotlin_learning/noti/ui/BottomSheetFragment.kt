package com.example.kotlin_learning.noti.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.kotlin_learning.R
import com.example.kotlin_learning.noti.interfaces.OnDeleteNotification
import com.example.kotlin_learning.noti.data.Notification
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.bottom_sheet_notification.*

class BottomSheetFragment(val notification: Notification) : BottomSheetDialogFragment() {

    var onDeleteNotification: OnDeleteNotification ? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.bottom_sheet_notification, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tvOptionTitle.text = notification.title
        tvOptionMessage.text = notification.message
        tvOptionTime.text = notification.time

        btnDeleteNotification.setOnClickListener{
            onDeleteNotification?.onDeleteNotification( notification)
        }
    }

    fun setOnDeletenotification(onDeleteNotification: OnDeleteNotification){
        this.onDeleteNotification = onDeleteNotification
    }
}