package com.example.kotlin_learning.noti.interfaces

import com.example.kotlin_learning.noti.data.Notification

interface OnDeleteNotification {
fun onDeleteNotification(notification: Notification)

}