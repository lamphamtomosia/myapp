package com.example.kotlin_learning.noti.data

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.example.kotlin_learning.noti.interfaces.OnNotiReceive
import com.example.kotlin_learning.utils.Define


class NotificationReceiver : BroadcastReceiver() {

    var onNotiReceive: OnNotiReceive? = null

    override fun onReceive(context: Context?, intent: Intent?) {
        Log.wtf("Notification ", "Received")

        if (intent!!.action.equals(Define.ACTION_BROADCAST_NOTIFICATION)) {
            //Đọc dữ liệu trong Intent
            val notification = intent.extras?.getParcelable<Notification>("notification")
            var db = context?.let { AppDatabase.getInMemoryDatabase(it) }
            var dao = db?.notiDao()

            notification?.let {
                dao?.insertAll(it)
            }
            if (onNotiReceive != null) onNotiReceive?.sendNotification()
            Log.wtf(
                "Notification ",
                "${notification?.title} ${notification?.message} ${notification?.time}"
            )
        }
    }

    fun setOnNotificationReceive(onNotiReceive: OnNotiReceive) {
        this.onNotiReceive = onNotiReceive
    }
}