package com.example.kotlin_learning.noti.interfaces

import com.example.kotlin_learning.noti.data.Notification

interface OnNotificationClick {
    fun onNotificationClicked(notification: Notification?)
}