package com.example.kotlin_learning.noti.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.kotlin_learning.R
import com.example.kotlin_learning.noti.data.Notification
import com.example.kotlin_learning.noti.interfaces.OnNotificationClick
import com.example.kotlin_learning.noti.interfaces.OnNotificationOptionClick
import kotlinx.android.synthetic.main.item_notification_recyclerview.view.*

class NotificationRecyclerViewAdapter(
    val context: Context,
    private var notifications: List<Notification>,
    val onNotificationClick: OnNotificationClick,
    val onNotificationOptionClick: OnNotificationOptionClick
) :
    RecyclerView.Adapter<NotificationRecyclerViewAdapter.NotificationViewHolder>() {
    class NotificationViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationViewHolder =
        NotificationViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_notification_recyclerview, parent, false)
        )

    @SuppressLint("ResourceAsColor", "ResourceType")
    override fun onBindViewHolder(holder: NotificationViewHolder, position: Int) {
        val notification = notifications[position]
        holder.itemView.run {
            tvNotificationTitle.text = notification.title
            tvNotificationMessage.text = notification.message
            tvNotificationTime.text = notification.time
            if (notification.status == true) notificationLayoutView.background = resources.getDrawable(R.drawable.read_notification_border)
            else notificationLayoutView.background = resources.getDrawable(R.drawable.unread_notification_border)

        }

        holder.itemView.notificationLayoutView.setOnClickListener {
            notification.status = true
            onNotificationClick.onNotificationClicked(notification)
            Log.wtf("Item Clicked", "${notification.id} + ${notification.status}")
        }
        holder.itemView.tvNotificationTitle.setOnClickListener {
            notification.status = true
            onNotificationClick.onNotificationClicked(notification)
            Log.wtf("Item Clicked", "${notification.id} + ${notification.status}")
        }
        holder.itemView.tvNotificationMessage.setOnClickListener {
            notification.status = true
            onNotificationClick.onNotificationClicked(notification)
            Log.wtf("Item Clicked", "${notification.id} + ${notification.status}")
        }
        holder.itemView.tvNotificationTime.setOnClickListener {
            notification.status = true
            onNotificationClick.onNotificationClicked(notification)
            Log.wtf("Item Clicked", "${notification.id} + ${notification.status}")
        }
        holder.itemView.tvNotificationOptions.setOnClickListener{
            onNotificationOptionClick.onOptionClick(notification)
            Log.wtf("Option Clicked", "${notification.id} + ${notification.status}")
        }

    }

    override fun getItemCount(): Int {
        return notifications.size
    }

    fun notify(notifications: List<Notification>){
        this.notifications = notifications
        notifyDataSetChanged()
    }

}