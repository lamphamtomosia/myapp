package com.example.kotlin_learning.home.data


data class UnsplashResponse(
    val results: List<UnsplashPhoto>
)