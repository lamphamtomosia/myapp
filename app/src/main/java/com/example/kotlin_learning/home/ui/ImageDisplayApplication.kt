package com.example.kotlin_learning.home.ui

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ImageDisplayApplication: Application() {
}