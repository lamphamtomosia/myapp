package com.example.kotlin_learning.login.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class User (var firstName: String?, var lastName: String?, var username: String?, var password: String?): Parcelable{
    override fun toString(): String {
        return "User [firstname: ${this.firstName}, lastname: ${this.lastName}, username: ${this.username}, password: ${this.password}]"
    }
}