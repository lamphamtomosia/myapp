package com.example.kotlin_learning.login.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.example.kotlin_learning.activities.MainActivity
import com.example.kotlin_learning.R
import com.example.kotlin_learning.utils.Validate
import kotlinx.android.synthetic.main.fragment_login.*


/**
 * A simple [Fragment] subclass.
 * Use the [LoginFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class LoginFragment : Fragment(), View.OnClickListener {

    var navController: NavController? = null
    val TAG = LoginFragment::class.java.name
    var validate: Validate? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        validate = Validate()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        Log.d(TAG, "On createView")
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val authorization= activity?.getSharedPreferences (resources.getString(R.string.authorization), AppCompatActivity.MODE_PRIVATE)
        navController = Navigation.findNavController(view)


        Log.d("Login fragment :", "On View Created")

        txtSignUp.setOnClickListener(this)
        btnLogin.setOnClickListener(this)
        loginBackground.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btnLogin -> {
                var intent = Intent(context, MainActivity::class.java)
                intent.putExtra("name", inputLoginUsername.text.toString())
                startActivity(intent)
            }

            R.id.txtSignUp -> navController!!.navigate(R.id.action_loginFragment_to_signUpNameFragment)

            R.id.loginBackground -> {
                val imm =
                    activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(view?.windowToken, 0)
            }
        }
    }

}