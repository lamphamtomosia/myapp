package com.example.kotlin_learning.login.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.example.kotlin_learning.activities.MainActivity
import com.example.kotlin_learning.login.data.User
import com.example.kotlin_learning.R
import com.example.kotlin_learning.utils.Define
import com.example.kotlin_learning.utils.Validate
import kotlinx.android.synthetic.main.fragment_sign_up_username.*
import kotlinx.android.synthetic.main.fragment_sign_up_username.btnBack
import kotlinx.android.synthetic.main.fragment_sign_up_username.btnNext
import kotlinx.android.synthetic.main.fragment_sign_up_username.signupBackground
import org.jetbrains.anko.support.v4.toast


class SignUpUsernameFragment : Fragment(), View.OnClickListener {

    var validate: Validate? = null
    var navController: NavController? = null
    val TAG = LoginFragment::class.java.name
    lateinit var user: User

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        validate = Validate()
        user = requireArguments().getParcelable<User>("person")!!

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sign_up_username, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = Navigation.findNavController(view)

        btnBack.setOnClickListener(this)
        btnNext.setOnClickListener(this)
        signupBackground.setOnClickListener(this)


    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btnBack -> activity?.onBackPressed()

            R.id.btnNext -> {
                if (validate!!.checkInputUsername(inputRegisterUsername.text.toString()) == null) {

                    if (validate!!.checkInputUsername(inputRegisterPassword.text.toString()) == null) {

                        if (validate!!.checkInputUsername(inputRegisterRePassword.text.toString()) == null) {

                            if (inputRegisterPassword.text.toString()
                                    .equals(inputRegisterRePassword.text.toString())
                            ) {
                                user.username = inputRegisterUsername.text.toString()
                                user.password = inputRegisterPassword.text.toString()

                                val authorization = activity?.getSharedPreferences(resources.getString(R.string.authorization),
                                    AppCompatActivity.MODE_PRIVATE
                                )


                                var commitResult = authorization!!.edit().putString(resources.getString(R.string.author), "${Define.LOGGED_IN_STATUS}").commit()

                                if(commitResult){
                                    var intent = Intent(context, MainActivity::class.java)
                                    intent.putExtra("person", user)

                                    startActivity(intent)
                                }else toast("Commit failed")

                            } else toast("Password Must be same")

                        } else toast(validate!!.checkInputUsername(inputRegisterRePassword.text.toString())!!)

                    } else toast(validate!!.checkInputUsername(inputRegisterPassword.text.toString())!!)

                } else toast(validate!!.checkInputUsername(inputRegisterUsername.text.toString())!!)

            }

            R.id.signupBackground -> {
                val imm =
                    activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(view?.windowToken, 0)
            }
        }
    }

}