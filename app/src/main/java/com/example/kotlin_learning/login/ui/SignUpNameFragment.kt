package com.example.kotlin_learning.login.ui

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.example.kotlin_learning.login.data.User
import com.example.kotlin_learning.R
import com.example.kotlin_learning.utils.Validate
import kotlinx.android.synthetic.main.fragment_sign_up_name.*
import org.jetbrains.anko.support.v4.toast


class SignUpNameFragment : Fragment(), View.OnClickListener {

    var validate: Validate? = null
    var navController: NavController? = null
    lateinit var user: User

    val TAG = LoginFragment::class.java.name

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        validate = Validate()

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sign_up_name, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = Navigation.findNavController(view)


        btnBack.setOnClickListener(this)
        btnNext.setOnClickListener(this)
        signupBackground.setOnClickListener(this)

        inputRegisterFirstName.addTextChangedListener {
            tvErrorRegisterFirstName.visibility = View.INVISIBLE
            if (inputRegisterFirstName.text.length < 3) {
                tvErrorRegisterFirstName.text = "Name must more than 3 characters"
                tvErrorRegisterFirstName.visibility = View.VISIBLE
            }
        }
        inputRegisterLastName.addTextChangedListener {
            tvErrorRegisterLastName.visibility = View.INVISIBLE
            if (inputRegisterFirstName.text.length < 3) {
                tvErrorRegisterLastName.text = "Name must more than 3 characters"
                tvErrorRegisterLastName.visibility = View.VISIBLE
            }
        }
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btnNext -> {
                Log.wtf(TAG, "Button Next Sign Up Name Clicked")
                if (validate!!.checkInputName(inputRegisterFirstName.text.toString()) == null) {
                    if (validate!!.checkInputName(inputRegisterLastName.text.toString()) == null) {

                        user = User(null, null, null, null)
                        user.firstName = inputRegisterFirstName.text.toString()
                        user.lastName = inputRegisterLastName.text.toString()

                        val bundle = Bundle()

                        bundle.putParcelable("person", user)

                        navController!!.navigate(
                            R.id.action_signUpNameFragment_to_signUpUsernameFragment,
                            bundle
                        )
                    } else toast(validate!!.checkInputName(inputRegisterLastName.text.toString())!!)
                } else toast(validate!!.checkInputName(inputRegisterFirstName.text.toString())!!)
            }

            R.id.btnBack -> {
                requireActivity().onBackPressed()
            }

            R.id.signupBackground -> {
                val imm =
                    activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(view?.windowToken, 0)
            }
        }
    }

}