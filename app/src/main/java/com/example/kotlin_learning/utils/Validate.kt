package com.example.kotlin_learning.utils

class Validate {
    fun checkInputUsername(inputString: String) : String? {
        val regex: Regex = "/^[A-Za-z0-9]+(?:[ _-][A-Za-z0-9]+)*\$/".toRegex()

        if (inputString.isNullOrEmpty() || inputString.isBlank()) return "Please enter username"

        if (inputString.length < 8) return "Minimum length is 8 characters"

        if (regex.matches(inputString)) return "Username contains only a-z 0-9"

        return null
    }

    fun checkInputName(inputString: String): String? {
        val regex: Regex = "/^[A-Za-z0-9]+(?:[ _-][A-Za-z0-9]+)*\$/".toRegex()

        if (inputString.isNullOrEmpty() || inputString.isBlank()) return "This field is required"

        if (inputString.length < 3) return "Minimum length is 3 characters"

        if (regex.matches(inputString)) return "Username contains only a-z 0-9"
        return null
    }


}