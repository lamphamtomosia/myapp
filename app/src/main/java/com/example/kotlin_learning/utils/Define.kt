package com.example.kotlin_learning.utils

import com.example.kotlin_learning.category.data.Category

class Define() {
    companion object{
        public const val LOGGED_IN_STATUS: Int = 1
        public const val LOG_OUT_STATUS: Int = 0
        const val CATEGORY_CAT_IMAGE_URL: String = "https://images.unsplash.com/photo-1570824104453-508955ab713e?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjE2OTc1OH0"
        const val CATEGORY_DOG_IMAGE_URL: String = "https://images.unsplash.com/photo-1510771463146-e89e6e86560e?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjE2OTc1OH0"
        const val CATEGORY_SEXY_IMAGE_URL: String = "https://images.unsplash.com/photo-1567892320421-1c657571ea4a?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjE2OTc1OH0"
        const val CATEGORY_FUNNY_IMAGE_URL: String = "https://images.unsplash.com/photo-1469598614039-ccfeb0a21111?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjE2OTc1OH0"
        const val CATEGORY_COSPLAY_IMAGE_URL: String = "https://images.unsplash.com/photo-1575454723382-16899c8ae4e1?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjE2OTc1OH0"
        const val CATEGORY_SCARY_IMAGE_URL: String = "https://images.unsplash.com/photo-1504701954957-2010ec3bcec1?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjE2OTc1OH0"
        val categories = arrayListOf(
            Category("Cats", CATEGORY_CAT_IMAGE_URL, "cats"),
            Category("Dogs", CATEGORY_DOG_IMAGE_URL, "dogs"),
            Category("Funny", CATEGORY_FUNNY_IMAGE_URL, "funny"),
            Category("Scary", CATEGORY_SCARY_IMAGE_URL, "scary"),
            Category("Cosplay", CATEGORY_COSPLAY_IMAGE_URL, "cosplay"),
            Category("Sexy", CATEGORY_SEXY_IMAGE_URL, "sexy")
        )
        const val ACTION_BROADCAST_NOTIFICATION = "notification-broadcast"
    }
}